﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Autofac.Extras.Moq;
using Microsoft.AspNetCore.Mvc;
using Shouldly;
using Xunit;
using Xunit.Abstractions;

namespace BHP.Test.Host.Controllers
{
    public class ServiceControllerTests
    {
        private readonly ITestOutputHelper _output;

        public ServiceControllerTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public void CanGetNow()
        {
            var dateService = new Services.DateTimeService();
            var controller = new DateTimeController(dateService);
            var response = (OkObjectResult) controller.Get();
            response.ShouldNotBeNull();
        }

        [Fact]
        public void CanGetNowViaMoq()
        {
            var now = DateTimeOffset.Now;

            using (var mock = AutoMock.GetLoose())
            {
                // Arrange //
                mock.Mock<Services.IDateTimeService>().Setup(o => o.Now).Returns(now);
                var controller = mock.Create<DateTimeController>();

                // Act //
                var response = (OkObjectResult) controller.Get();

                // Assert //
                mock.Mock<Services.IDateTimeService>().Verify(o => o.Now);
                response.Value.ShouldBe(now.ToString("o"));

                _output.WriteLine($"{response.Value}");
            }
        }
    }
}

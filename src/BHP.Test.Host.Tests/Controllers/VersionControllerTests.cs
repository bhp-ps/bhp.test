﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Shouldly;
using Xunit;
using Xunit.Abstractions;

namespace BHP.Test.Host.Controllers
{
    public class VersionControllerTests
    {
        private readonly ITestOutputHelper _output;

        public VersionControllerTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public void CanGetVersion()
        {
            var controller = new VersionController();
            var response = (OkObjectResult)controller.GetVersion();
            var version = typeof(VersionController).Assembly.GetName().Version.ToString();
            response.ShouldNotBeNull();
            response.Value.ShouldBe(version);
            _output.WriteLine($"{response.Value}");
        }
    }
}

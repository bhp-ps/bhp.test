﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Shouldly;
using Xunit;
using Xunit.Abstractions;

namespace BHP.Test.Host.Services
{
    public class DateServiceTests
    {
        private readonly ITestOutputHelper _output;

        public DateServiceTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public async Task CanGetDate()
        {
            var dateService = new DateTimeService();

            var before = DateTimeOffset.Now;
            var after = DateTimeOffset.Now;

            _output.WriteLine($"Before: {before.ToString("o")}");
            await Task.Delay(10);
            _output.WriteLine($"After: {after.ToString("o")}");

            after.ShouldBeGreaterThan(before);

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;

namespace BHP.Test.Host
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        private const string ApiVersion = "v1";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public Options Options { get; set; } = new Options();

        public string ApplicationName => this.GetType().Assembly.GetName().Name;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Options //
            services.AddOptions();
            services.Configure<Options>(this.Configuration);
            services.AddSingleton((IConfigurationRoot)Configuration);

            // Swagger / OpenApi //
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(ApiVersion, new Info
                {
                    Title = this.ApplicationName,
                    Version = ApiVersion
                });
            });
            
            // Health //
            services.AddHealthChecks();
            
            // MVC //
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime (after ConfigureServices) and is used to configure Autofac //
        public virtual void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new DefaultModule() { Configuration = this.Configuration });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            
            // Health //            
            app.UseHealthChecks("/healthz"); // Kubernetes convention for health checks //
            app.UseHealthChecks("/api/health");           

            // Swagger / OpenApi //
            app.UseSwagger();
            app.UseSwaggerUI(o =>
            {
                o.SwaggerEndpoint($"/swagger/{ApiVersion}/swagger.json", $"{this.ApplicationName} {ApiVersion}");
            });

            // MVC //
            app.UseMvc();
        }
    }
}

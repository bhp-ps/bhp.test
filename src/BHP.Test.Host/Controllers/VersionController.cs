﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace BHP.Test.Host.Controllers
{
    [Route("api/version")]
    [ProducesResponseType(typeof(string), 200)]
    public class VersionController : ControllerBase
    {
        [HttpGet]
        public IActionResult GetVersion()
        {
            var version = this.GetType().Assembly.GetName().Version.ToString();
            return this.Ok(version);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BHP.Test.Host.Services;
using Microsoft.AspNetCore.Mvc;

namespace BHP.Test.Host.Controllers
{
    [Route("api/datetime")]
    [ApiController]
    public class DateTimeController : ControllerBase
    {
        private readonly IDateTimeService _dateTimeService;

        public DateTimeController(IDateTimeService dateTimeService)
        {
            _dateTimeService = dateTimeService;
        }

        [HttpGet("")]
        [ProducesResponseType(typeof(string), 200)]
        public IActionResult Get()
        {
            return this.Ok(_dateTimeService.Now.ToString("o"));
        }
    }
}

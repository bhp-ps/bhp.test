﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BHP.Test.Host.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Design;

namespace BHP.Test.Host.Controllers
{
    [Route("api/prime")]
    [ApiController]
    public class PrimeController : ControllerBase
    {
        /// <summary>
        /// Gets the next prime number
        /// </summary>
        [HttpGet("")]
        [ProducesResponseType(typeof(long), 200)]
        public IActionResult Get()
        {
            return this.Ok(0);
        }
    }
}

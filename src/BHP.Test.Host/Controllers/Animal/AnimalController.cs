﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BHP.Test.Host.Services;
using Microsoft.AspNetCore.Mvc;

namespace BHP.Test.Host.Controllers.Animal
{
    [Route("api/animal")]
    public class AnimalController : ControllerBase
    {
        private readonly IAnimalRepository _animalRepository;
        
        public AnimalController(IAnimalRepository animalRepository)
        {
            _animalRepository = animalRepository;
        }
        
        [HttpGet("")]
        [ProducesResponseType(typeof(FindBySpecificationResponse), 200)]
        public IActionResult FindBySpecification([FromQuery]FindBySpecificationRequest request)
        {
            var response = new FindBySpecificationResponse();
            return this.Ok(response);
        }
    }
}

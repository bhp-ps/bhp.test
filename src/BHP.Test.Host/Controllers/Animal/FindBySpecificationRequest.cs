using BHP.Test.Host.Services;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BHP.Test.Host.Controllers.Animal
{
    public class FindBySpecificationRequest : IPageable
    {
        public string CommonNameStartsWith { get; set; }
        
        public string ClassNameStartsWith { get; set; }
        
        public string ScientificNameStartsWith { get; set; }
        
        public string Habitat { get; set; }

        public int PageSize { get; set; } = 10;

        public int PageNumber { get; set; } = 1;
    }
}
using System.Collections.Generic;
using BHP.Test.Host.Services;

namespace BHP.Test.Host.Controllers.Animal
{
    public class FindBySpecificationResponse
    {
        /// <summary>
        /// The filtered set of animals
        /// </summary>
        public List<Domain.Animal> Animals { get; set; }
        
        /// <summary>
        /// The total number of animals (post filtering but pre paging).
        /// </summary>
        public long Total { get; set; }
    }
}
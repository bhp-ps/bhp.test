﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BHP.Test.Host.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Design;

namespace BHP.Test.Host.Controllers
{
    [Route("api/duration")]
    [ApiController]
    public class DurationController : ControllerBase
    {
        [HttpGet("{duration}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(string), 200)]
        public IActionResult Get(string duration)
        {
            return this.Ok("");
        }
    }
}

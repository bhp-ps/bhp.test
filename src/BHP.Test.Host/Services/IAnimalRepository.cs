using System.Collections.Generic;

namespace BHP.Test.Host.Services
{
    public interface IAnimalRepository
    {
        List<Domain.Animal> GetAnimals();
    }
}
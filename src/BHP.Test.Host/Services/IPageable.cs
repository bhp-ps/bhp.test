namespace BHP.Test.Host.Services
{
    public interface IPageable
    {
        int PageSize { get; set; }
        
        int PageNumber { get; set; }
    }
}
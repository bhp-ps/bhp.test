﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BHP.Test.Host.Services
{
    public interface IDateTimeService
    {
        DateTimeOffset Now { get; }
    }
}

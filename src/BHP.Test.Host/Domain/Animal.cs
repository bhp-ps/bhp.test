namespace BHP.Test.Host.Domain
{
    public class Animal
    {
        public string CommonName { get; set; }
        
        public string ClassName { get; set; }
        
        public string ScientificName { get; set; }
        
        public string Habitat { get; set; }
    }
}
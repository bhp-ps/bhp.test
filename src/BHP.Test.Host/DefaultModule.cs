﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Microsoft.Extensions.Configuration;

namespace BHP.Test.Host
{
    public class DefaultModule : Autofac.Module
    {
        public IConfiguration Configuration { get; set; }

        protected override void Load(ContainerBuilder builder)
        {
            /*/
            var assembly = Assembly.GetEntryAssembly();
            var assemblies = AppDomain.CurrentDomain.GetAssemblies()
                .Where(a => a.FullName.StartsWith("BHP."))
                .Union(new[] { assembly })
                .ToArray();

            builder.RegisterAssemblyTypes(assemblies).AsImplementedInterfaces().AsSelf();
            //*/

            builder.RegisterType<Services.DateTimeService>().AsImplementedInterfaces();
            builder.RegisterType<Services.AnimalRepository>().AsImplementedInterfaces();

            base.Load(builder);
        }
    }
}

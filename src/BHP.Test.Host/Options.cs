﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace BHP.Test.Host
{
    [ExcludeFromCodeCoverage]
    public class Options
    {
        public string Env { get; set; }
    }
}

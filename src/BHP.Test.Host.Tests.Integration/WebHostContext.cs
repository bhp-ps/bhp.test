﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;

namespace BHP.Test.Host
{
    public class WebHostContext
    {
        private TestServer _server;

        public HttpClient Client { get; private set; }

        public WebHostContext()
        {
            SetUpClient();
        }

        private void SetUpClient()
        {
            var webHostBuilder = Program.CreateWebHostBuilder(new string[0]);
            _server = new TestServer(webHostBuilder);
            this.Client = _server.CreateClient();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Shouldly;
using Xunit;
using Xunit.Abstractions;

namespace BHP.Test.Host.Controllers
{
    public class VersionControllerTests
    {
        private readonly ITestOutputHelper _output;
        private readonly WebHostContext _hostContext;

        public VersionControllerTests(ITestOutputHelper output)
        {
            _output = output;
            _hostContext = new WebHostContext();
        }

        [Fact]
        public async Task GetVersionsReturnsOkResponse()
        {
            var response = await _hostContext.Client.GetAsync("/api/version");
            response.EnsureSuccessStatusCode();
            response.StatusCode.ShouldBe(HttpStatusCode.OK);

            var content = await response.Content.ReadAsStringAsync();
            _output.WriteLine($"Status: {response.StatusCode}");
            _output.WriteLine($"Content: {content}");
        }
    }
}



### Implement the Get action of the DurationController such that

* The duration parameter should be of the format hh:mm where hh is hours and mm is minutes.
* A duration with an invalid format should return a status of 400.
* A duration with a valid format should return the written form of the duration as per the following examples...
  * 00:20 should return "twenty minutes"
  * 01:01 should return "one hour and one minute".
  * 02:20 should return "two hours and twenty minutes".
  * 11:59 should return "eleven hours and 59 minutes".

### Implement the Next action of the PrimeController

* The Next action should return the next prime number in the sequence regardless of which user makes the request (2, 3, 5, 7, 11, 13, ...)
* The sequence should reset when the application is restarted (ie. You don't need to persist the sequence value between application restarts).
* This should be implemented without using the static keyword.
* Your solution should be thread safe.

### Implement Find action of the AnimalController

* Allow filtering of search results by each property of FindBySpecificationRequest.
  * If CommonNameStartsWith is specified then only animals where CommonName starts with this text should be returned.
  * If ClassNameStartsWith is specified then only animals where ClassName starts with this text should be returned.
  * If ScientificNameStartsWith is specified then only animals where ScientificNameStartsWith starts with this text should be returned.
  * If Habitat is specified then only animals where Habitat matches this text should be returned.
  * Page should be used to implement filtering for the purpose of paging (Ideally the implementation of this should be reusable)
* Ideally the implementation of filtering for paging should be reusable.
* 



